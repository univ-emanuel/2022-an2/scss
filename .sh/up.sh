#!/bin/bash

export CURRENT_UID=$(id -u):$(id -g)
(
  set -x
  docker compose up -d
)
(
  set -x
  docker compose exec curs6 sh
)
